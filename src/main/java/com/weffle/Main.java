package com.weffle;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        primaryStage.setTitle("Cuckmod");

        Parent root = FXMLLoader.load(
                Main.class.getResource("/fxml/dashboard.fxml"));
        Scene scene = new Scene(root, 200, 192);
        primaryStage.setScene(scene);

        primaryStage.setMaximized(false);
        primaryStage.setResizable(false);
        primaryStage.show();

        primaryStage.setOnCloseRequest(event -> System.exit(0));
    }

    public static void main(String[] args) {
        launch(args);
    }
}
