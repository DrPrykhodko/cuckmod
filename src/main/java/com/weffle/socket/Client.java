package com.weffle.socket;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.function.Consumer;

public class Client {
    private Socket socket;

    public Client(Consumer<String> consumer) {
        try {
            socket = new Socket(InetAddress.getByName("85.238.96.66"), 1488);
            DataInputStream input =
                    new DataInputStream(socket.getInputStream());
            new Thread(() -> {
                while (!Thread.currentThread().isInterrupted() &&
                        !socket.isClosed()) {
                    try {
                        consumer.accept(input.readUTF());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }, "Client thread").start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        if (socket != null && !socket.isClosed()) {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
