package com.weffle.controller;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.Charset;

public class Donation {

    @FXML
    private TextField UsernameField;

    @FXML
    private CheckBox UsernameBox;

    @FXML
    private TextArea MessageArea;

    @FXML
    private CheckBox MessageBox;

    @FXML
    private Label Label;

    @FXML
    private ProgressBar ProgressBar;

    private final String ACCEPT_ENDPOINT = "https://cuckmod.weffle.com/leyagornaya/cuckmod/accept";

    private Stage stage;
    private JSONObject donation;
    private static boolean busy;
    private Thread timeoutThread;

    public void initialize() {
        busy = true;
    }

    @FXML
    void onAccept() {
        timeoutThread.interrupt();
        ProgressBar.setProgress(-1);
        new Thread(() -> {
            CloseableHttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(ACCEPT_ENDPOINT);
            post.setHeader("Content-Type", "application/json;charset=utf8");
            post.setEntity(new StringEntity(getDonation().toString(),
                    Charset.forName("UTF-8")));
            System.out.println(getDonation().toString());
            try {
                CloseableHttpResponse response = client.execute(post);
                System.out.println(response.getStatusLine().getStatusCode());
            } catch (IOException e) {
                e.printStackTrace();
            }
            close();
        }).start();
    }

    @FXML
    void onReject() {
        close();
    }

    private void close() {
        busy = false;
        Platform.runLater(() -> {
            if (stage.isShowing())
                stage.close();
            Dashboard.update();
        });
    }

    void setStage(Stage stage, long timeout) {
        this.stage = stage;
        this.stage.setOnCloseRequest(event -> close());
        setTimeout(timeout);
    }

    private void setTimeout(long timeout) {
        timeoutThread = new Thread(() -> {
            long start = System.currentTimeMillis();
            while (!Thread.currentThread().isInterrupted() &&
                    stage.isShowing() &&
                    start + timeout > System.currentTimeMillis()) {
                Platform.runLater(() ->
                {
                    double progress = 1.0 - (System.currentTimeMillis() -
                            start) / (double) timeout;
                    ProgressBar.setProgress(progress);
                });
                try {
                    Thread.sleep(250);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    Thread.currentThread().interrupt();
                }
            }
            onReject();
        });
        timeoutThread.start();
    }

    void setDonation(JSONObject donation) {
        this.donation = donation;
        parse(donation);
    }

    private void parse(JSONObject donation) {
        UsernameField.setText(donation.getString("username"));
        MessageArea.setText(donation.getString("message"));
        Label.setText(String.format("%s %s",
                donation.getString("amount_formatted"),
                donation.getString("currency")));
    }

    private JSONObject getDonation() {
        JSONObject donation = new JSONObject(this.donation.toString());
        donation.put("username", UsernameBox.isSelected() ?
                UsernameField.getText().trim() : "Аноним");
        donation.put("message", MessageBox.isSelected() ?
                MessageArea.getText().trim() : "");
        return donation;
    }

    @FXML
    void onMessageBox() {
        MessageArea.setText(MessageBox.isSelected() ?
                donation.getString("message") : "");
    }

    @FXML
    void onUsernameBox() {
        UsernameField.setText(UsernameBox.isSelected() ?
                donation.getString("username") : "");
    }

    static boolean isBusy() {
        return busy;
    }
}
