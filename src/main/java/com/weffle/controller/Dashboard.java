package com.weffle.controller;

import com.weffle.Main;
import com.weffle.socket.Client;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayDeque;
import java.util.Random;

public class Dashboard {
    private Client client;

    @FXML
    private ImageView Image;

    @FXML
    private Label Label;

    @FXML
    private ToggleButton Launch;

    private static ArrayDeque<JSONObject> tasks;

    public void initialize() throws URISyntaxException {
        Image.setImage(new Image(Main.class.getResource(String.format(
                "/image/cactus_%d.png",
                new Random().nextInt(6))).toURI().toString()));
        Label.setText("Добро пожаловать");
        tasks = new ArrayDeque<>();
    }

    @FXML
    void onLaunch() {
        if (Launch.isSelected()) {
            Label.setText("Удачного стрима!");
            Launch.setText("Остановить");

            client = new Client(json -> {
                tasks.add(new JSONObject(json));
                update();
            });
        } else {
            Label.setText("Спасибо за стрим!");
            Launch.setText("Запустить");

            if (client != null)
                client.close();
        }
    }

    synchronized static void update() {
        if (Donation.isBusy())
            return;
        if (tasks.size() > 0)
            Platform.runLater(() -> {
                try {
                    loadDonation(tasks.element());
                    tasks.remove();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
    }

    private static void loadDonation(JSONObject donationJson) throws Exception {
        Stage stage = new Stage();
        stage.setTitle("Cuckmod - Donation");
        stage.setMaximized(false);
        stage.setResizable(false);

        FXMLLoader loader = new FXMLLoader(
                Main.class.getResource("/fxml/donation.fxml"));
        Scene scene = new Scene(loader.load(), 400, 276);
        Donation donation = loader.getController();
        donation.setDonation(donationJson);
        stage.setScene(scene);
        stage.setAlwaysOnTop(true);
        donation.setStage(stage, 60_000L);
        stage.show();
    }
}
